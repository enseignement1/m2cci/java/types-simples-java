
# Correction du [TP JAVA : Types simples et instructions de base](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp03_typesSimples/typesSimples.html)

## Release 1.0

Ce projet contient le code source (sous la forme d'un projet maven d'application java) d'une correction des différents exercices du [TP Types simples et instructions de bases Java)](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp03_typesSimples/typesSimples.html) 
du cours Java du M2CCI.

Pour récupérer les sources de ce projet vous pouvez

* soit utiliser git en effectuant la commande 


   ```
   git clonehttps://gricad-gitlab.univ-grenoble-alpes.fr/enseignement1/m2cci/java/types-simples-java.git
   ```

   ou bien (si vous avez un accès SSH)

   ```
   git clone git@gricad-gitlab.univ-grenoble-alpes.fr:enseignement1/m2cci/java/correctiontp2.git
   ```

* soit récupérer une archive (.tar.gz ou .zip) de cette release 1.0 : [TypesSimples v1.0](https://gricad-gitlab.univ-grenoble-alpes.fr/enseignement1/m2cci/java/types-simples-java/-/releases)



