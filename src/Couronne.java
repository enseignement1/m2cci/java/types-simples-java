import java.util.Scanner;

/**
 * Couronne.java
 *
 * détermine si un point P du plan se trouve ou non à l'intérieur de la couronne
 * de centre l'origine et définie par la donnée de son rayon extérieur r1 et de
 * son rayon intérieur r2.
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
public class Couronne {

    public static void main(String[] args) {

        // pour régler les éventuels problèmes d'affichage des caractères accentués sur la console
        Utils.fixEncoding();

        Scanner sc = new Scanner(System.in);
        double rInterieur; // rayon intérieur de la couronne
        double rExterieur; // rayon extérieur de la couronne
        System.out.println("entrez le rayon intérieur de la couronne : ");
        rInterieur = sc.nextDouble();

        System.out.println("entrez le rayon extérieur de la couronne : ");
        rExterieur = sc.nextDouble();

        double x, y; // les coordonnées du point

        System.out.println("entrez l'abscisse du point : ");
        x = sc.nextDouble();
        System.out.println("entrez l'ordonnée du point : ");
        y = sc.nextDouble();

        double dist = (x * x) + (y * y); // le carre de la distance à l'origine

        if ((dist >= rInterieur * rInterieur) && (dist <= rExterieur * rExterieur)) {
            System.out.println("Le point est dans la couronne");
        } else {
            System.out.println("Le point n'est pas dans la couronne");
        }

        sc.close();
    }

} // Couronne

