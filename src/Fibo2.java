import java.util.Scanner;

/**
 * Calcule le rang et la valeur du premier terme de la suite de Fibonacci,
 * supérieur à un entier n donné au clavier. La suite de fibonacci est définie
 * par la formule de recurence suivante : un = un-1 + un-2 et u1 = 1 et u2 = 2.
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
public class Fibo2 {

    public static void main(String[] args) {
        // pour régler les éventuels problèmes d'affichage des caractères accentués sur la console
        Utils.fixEncoding();

        Scanner sc = new Scanner(System.in);

        System.out.println("Donnez un nombre > 0 : ");
        int n = sc.nextInt();

        int fImoins1 = 1;  // Fibo(1);
        int fI = 1;        // Fibo(2);
        int i = 2;
        // fI = Fibo(i-1) + Fibo(i-2) = Fibo(i) et fIMoins1 = Fibo(i-1)
        while (fI <= n) {
            i++;
            // fI = Fibo(i-1) et fImoins1 = Fibo(i-2)
            int temp = fI;
            fI = fI + fImoins1;
            // fI = Fibo(i-1) + Fibo(i-2) = Fibo(i) et fIMoins1 = Fibo(i-2)
            fImoins1 = temp;
            // fI = Fibo(i-1) + Fibo(i-2) = Fibo(i) et fIMoins1 = Fibo(i-1)
        }

        System.out.println("le rang du premier terme de la suite de Fibonacci supérieur à " + n
                + " est: " + i);
        System.out.println("la valeur de ce terme est: " + fI);    
        sc.close();

    }
}
