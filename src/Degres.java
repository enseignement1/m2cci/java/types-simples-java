
import java.util.Scanner;

/**
 * Degres.java
 *
 * lit une température exprimée en degrés Fahrenheit et affiche sa valeur en
 * degrés Celsius.
 *
 *
 * @author <a href="mailto:Philippe.Genoud@imag.fr">Philippe Genoud</a>
 */
public class Degres {

    public static void main(String[] args) {
        // pour régler les éventuels problèmes d'affichage des caractères accentués sur la console
        Utils.fixEncoding();

        Scanner sc = new Scanner(System.in);

        System.out.println("entrez une température (en °F) :  ");
        double tempF = sc.nextFloat();

        double tempC =  (5.0 / 9) * (tempF - 32); // attention ! : il faut
        // bien écrire 5.0/9.0
        // et non pas 5/9 car sinon on effectue une
        // division entière et le résultat de l'expression
        // est alors toujours égal à 0

        System.out.println("cette température équivaut à °C : " + tempC);
        System.out.printf("cette température équivaut à %5.2f °C \n", tempC);
        sc.close();
    }

} // Degres

