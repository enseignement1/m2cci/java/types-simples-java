
import java.util.Scanner;

/**
 * Calcule la valeur du terme de rang n de la suite de Fibonacci, n donné au
 * clavier. La suite de fibonacci est définie par la formule de recurence
 * suivante : un = un-1 + un-2 et u1 = 1 et u2 = 2.
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
public class Fibo1 {

    public static void main(String[] args) {
        // pour régler les éventuels problèmes d'affichage des caractères accentués sur la console
        Utils.fixEncoding();
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Donnez un nombre > 0 : ");
        int n = sc.nextInt();

        if (n == 1) {
            System.out.println("Fibo(1) : " + 1);
        } else if (n == 2) {
            System.out.println("Fibo(2) : " + 1);
        } else {
            int fImoins1 = 1;  // Fibo(1)
            int fI = 1;        // Fibo(2)
            // fI = Fibo(2) et fIMoins1 = Fibo(1)

            for (int i = 3; i <= n; i++) {
                // fI = Fibo(i-1) et fImoins1 = Fibo(i-2)
                int temp = fI; // temp = Fibo(i-1)

                fI = fI + fImoins1;
                // fI = Fibo(i-1) + Fibo(i-2) = Fibo(i) et fIMoins1 = Fibo(i-2)

                fImoins1 = temp;
                // fI = Fibo(i-1) + Fibo(i-2) = Fibo(i) et fIMoins1 = Fibo(i-1)
            }

            System.out.printf("Fibo(%d) = %d\n", n, fI);
            sc.close();
        }

    }
}
