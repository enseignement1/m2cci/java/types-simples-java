
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Méthoddes utilitaires utilisées pour tous les exercices
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
public class Utils {

    /**
     * fix pour éviter problèmes d'encodage sur la console et les problèmes
     * d'affichage des caractères accentués. Appeler cette méthode avant tout
     * affichage sur la sortie standard
     */
    public static void fixEncoding() {
        if (!Charset.defaultCharset().equals(StandardCharsets.UTF_8)) {
            try {
                System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out), true, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                throw new InternalError("VM does not support mandatory encoding UTF-8");
            }
        }
    }
}
